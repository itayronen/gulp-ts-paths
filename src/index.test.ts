import { TestParams, TestSuite } from "just-test-api";
import { expect } from "chai";
import * as File from "vinyl";
import ObjectStream, { EnteredArgs, Transform } from "o-stream";
import * as PluginError from "plugin-error";
import plugin, { CompilerOptions, Params } from "./index";

type TestCase = {
	pluginParams: Params,
	filePath: string | undefined,
	input: string,
	expected: string
};

var compilerOptions: CompilerOptions = { paths: { "MyAlias": ["MyAliasFolder/MyAliasClass"] } };

export default function (suite: TestSuite): void {
	suite.test("Works with compilerOptions.", async test => {
		await runIntegrationTestCase({
			pluginParams: { config: compilerOptions },
			filePath: "./src/FileFolder/InnerFileFolder/File.ts",
			input: `
import A from "./asdf";
import B from "./MyAlias";
import C from "MyAlias";
import D from "express";
`,
			expected: `
import A from "./asdf";
import B from "./MyAlias";
import C from "../../MyAliasFolder/MyAliasClass";
import D from "express";
`
		});
	});

	suite.test("Works with tsconfig.", async test => {
		await runIntegrationTestCase({
			pluginParams: { config: { compilerOptions } },
			filePath: "./src/FileFolder/InnerFileFolder/File.ts",
			input: `
import A from "./asdf";
import B from "./MyAlias";
import C from "MyAlias";
`,
			expected: `
import A from "./asdf";
import B from "./MyAlias";
import C from "../../MyAliasFolder/MyAliasClass";
`
		});
	});

	suite.test("When no config provided, throws.", async test => {
		let error;

		try {
			await runIntegrationTestCase({
				pluginParams: { config: undefined! },
				filePath: undefined,
				input: "",
				expected: ""
			});
		}
		catch (e) {
			error = e;
		}

		expect(error).to.exist;
		expect(error).to.be.instanceof(PluginError);
	});

	suite.test("When file has no path, throws.", async test => {
		let error;

		try {
			await runIntegrationTestCase({
				pluginParams: { config: compilerOptions },
				filePath: undefined,
				input: "",
				expected: ""
			});
		}
		catch (e) {
			error = e;
		}

		expect(error).to.exist;
		expect(error).to.be.instanceof(PluginError);
	});
}

function runIntegrationTestCase(test: TestCase): Promise<void> {
	return new Promise<void>((resolve, reject) => {
		let file = new File({ contents: new Buffer(test.input), path: test.filePath });
		let stream = ObjectStream.fromArray([file]);

		stream.pipe(plugin({ config: compilerOptions, baseDir: "./src" })).pipe(ObjectStream.transform({
			onEntered: (args: EnteredArgs<File, void>) => {
				try {
					expect(args.object.contents!.toString()).to.equal(test.expected)
				}
				catch (reason) { reject(reason); }
			},
			onEnded: () => { resolve(); }
		})).on("error", (error) => reject(error));
	});
}
