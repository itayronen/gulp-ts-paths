"use strict";
let path = require("path");
let gulp = require("gulp");
let ts = require('gulp-typescript');
let sourcemaps = require('gulp-sourcemaps');
let read = require("gulp-read");
let del = require("del");
let changed = require("itay-gulp-changed");
let merge = require("merge2");

let config = {
	src: "./src",
	tsconfig: "./src/tsconfig.json",
	tsGlob: "./src/**/*.ts",
	dest: "./lib",
};

gulp.task("build", function () {
	let tsProject = ts.createProject(config.tsconfig, { declaration: true });

	let tsResult = gulp.src(config.tsGlob, { read: false })
		.pipe(changed())
		.pipe(read())
		.pipe(sourcemaps.init())
		.pipe(tsProject())
		;

	tsResult.on("error", () => {
		changed.reset();
		throw "Typescript build failed.";
	});

	tsResult.js = tsResult.js.pipe(sourcemaps.write({
		includeContent: false,
		sourceRoot: file => jsFileToSrcFolder(file)
	}));

	return merge([
		tsResult.dts.pipe(gulp.dest(config.dest)),
		tsResult.js.pipe(gulp.dest(config.dest))
	]);
});

function jsFileToSrcFolder(file) {
	let jsPath = path.join(config.dest, path.dirname(file.relative));

	return path.relative(jsPath, config.src);
}

gulp.task("clean", function () {
	return del([
		config.dest + '**/*',
		"./.localStorage"
	]);
});